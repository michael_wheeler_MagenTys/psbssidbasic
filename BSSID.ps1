while ($true) {

    $net = netsh wlan show interfaces
    $Script:networkInfo = ""

    foreach ($i in $net) { 
        if ($i -match "^\s+BSSID") {
            $BSSID = $i -replace "^\s+BSSID\s+:\s+", ""
            $networkInfo = $networkInfo + $BSSID + "_"
        }
        elseif ($i -match "^\s+Signal") {
            $signal = $i -replace '[^0-9]'
            $networkInfo = $networkInfo + $signal
        }
        $j=$j+1
    }
    $networkOut = (Get-date).ToString('dd-MM-yyyy_hh-mm,') + $networkInfo
    $networkOut | Out-File -FilePath 'C:\Users\Michael Wheeler\Desktop\BSSIDLOG.txt' -Append

    Start-sleep -seconds 60
}